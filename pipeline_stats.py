import os
import time
from datetime import datetime
from urllib import parse
from urllib.parse import urlencode
import dateutil.parser
import requests
import schedule
from dotenv import load_dotenv
from influxdb import InfluxDBClient

load_dotenv()

project_id = str(os.getenv('PROJECT_ID'))

client = InfluxDBClient(host=os.getenv('DATABASE_HOST'), port=os.getenv('DATABASE_PORT'),
                        username=os.getenv('DATABASE_USER'),
                        password=os.getenv('DATABASE_PASS'))

client.create_database("frontenddb")

client.switch_database("frontenddb")


def build_url(base_url, path, args_dict):
    url_parts = list(parse.urlparse(base_url))
    url_parts[2] = path
    url_parts[4] = urlencode(args_dict)
    return parse.urlunparse(url_parts)


def send_request():
    global current_time
    global current_date
    json_body = []

    page_num = 1
    repository_path = f'/api/v4/projects/{project_id}/pipelines'

    while page_num:

        args = {'per_page': '100', 'page': f'{page_num}'}
        base_url = f'https://gitlab.com/'
        url1 = build_url(base_url, repository_path, args)
        print(url1)
        json_body_ = requests.get(str(url1), headers={'PRIVATE-TOKEN': os.getenv('TOKEN')}).json()
        if not json_body_:
            break
        page_num = page_num + 1
        json_body.append(json_body_)

    ids = []
    ids_page_index = 0

    while ids_page_index < len(json_body):
        for ids_body_index in range(len(json_body[ids_page_index])):

            if json_body[ids_page_index][ids_body_index]['id'] not in ids:
                ids.append(json_body[ids_page_index][ids_body_index]['id'])

        ids_page_index = ids_page_index + 1

    json_body_1 = []

    for id in ids:
        repository_path_1 = f'/api/v4/projects/{project_id}/pipelines/{id}'

        args = {}
        base_url = f'https://gitlab.com/'
        url1 = build_url(base_url, repository_path_1, args)
        # print(url1)
        json_body_1_ = requests.get(str(url1), headers={'PRIVATE-TOKEN': os.getenv('TOKEN')}).json()
        if not json_body_1_:
            break
        # page_num_1 = page_num_1 + 1
        json_body_1.append(json_body_1_)

    my_json = []

    for json_body_index in range(len(json_body_1)):
        if json_body_1[json_body_index]['status'] == 'running':
            continue
        my_json.append({
            "measurement": "pipelines",
            "tags": {
                "username": json_body_1[json_body_index]['user']['username'],
                "status": json_body_1[json_body_index]['status'],
                "ref": json_body_1[json_body_index]['ref']
            },
            "time": json_body_1[json_body_index]['updated_at'],
            "fields": {
                "username_field": json_body_1[json_body_index]['user']['username'],
                "status_field": json_body_1[json_body_index]['status'],
                "ref_field": json_body_1[json_body_index]['ref'],
                "startTime": json_body_1[json_body_index]['started_at'],
                "finishTime": json_body_1[json_body_index]['finished_at'],
                "timeTaken": json_body_1[json_body_index]['duration'],
                "commitTime": json_body_1[json_body_index]['committed_at']
            }
        })




    client.write_points(my_json)
    now = datetime.now()
    current_time = str(now.time())[:-7]

    splits = current_time.split(":")
    current_time = str(int(splits[0]) - 5) + ":" + splits[1] + ":" + splits[2]
    current_date = now.date()


schedule.every(5).seconds.do(send_request)

while True:
    schedule.run_pending()
    time.sleep(1)
