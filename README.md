## STEPS TO DEPLOY THE GRAFANA DASHBOARD FOR GITLAB PIPELINES STATISTICS

#### Prerequisites
1. Docker
2. Docker Compose
3. Git
4. Python

#### Installation
1.  Clone into the repository and get inside the folder:
     ```sh
    sudo git clone https://gitlab.com/Osama93/gitlab-pipelines.git
    ```
    ```sh
    cd gitlab-pipelines/
    ```
2. Next, change the gitlab project id in the .env file. Go to the concerned gitlab project look for the "PROJECT ID: xxxxxxx" immediately below the project name. Copy the ID and replace it in the .env file for PROJECT_ID.
    
3. Make sure the ports 3000, 8086, 8083 and 8888 are not occupied. 

4. Now simply run the following command to deploy the dashboard:
   ```sh
   sudo docker-compose up -d
   ```
5. Go to the browser and enter http://ip_address:3000

6. On the grafana login page enter username: 'admin' and password: 'root'. These credentials can be changed in the docker-compose.yml file in the 'grafana' section and subsection 'environment', i.e. GF_SECURITY_ADMIN_USER and GF_SECURITY_ADMIN_PASSWORD.

7. Click on the dashboard named 'Gitlab Pipelines'.






